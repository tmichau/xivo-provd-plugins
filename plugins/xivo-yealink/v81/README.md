:warning: **Attention:** to install firmware for T4XS family, the 'unrar' executable must be
present on the host system. If it is not, you'll have a runtime error while
trying to install any firmware.

Starting from Five.08 and 2017.08 unrar-free plus unar will be installed as
a depency of xivo-fetchfw package.

Otherwise you need to install them manually :

```
apt-get update
apt-get install unrar-free unar
```
